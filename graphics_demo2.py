from graphics import *

def line(x1, y1, x2, y2):
    return Line(Point(x1, y1), Point(x2, y2))

def main():
    win = GraphWin("My Window", 500, 500)
    win.setBackground(color_rgb(0, 0, 0))
    
    # pt example 1
    pt = Point(250, 250)
    pt.setOutline(color_rgb(255, 255, 0))
    pt.draw(win)
    
    # line example1
    pt1 = Point(260, 260)
    pt2 = Point(260, 300)
    ln1 = Line(pt1, pt2)
    ln1.setOutline(color_rgb(0, 255, 255))
    ln1.draw(win)

    # line example2
    ln2 = Line(Point(270, 270), Point(270, 300))
    ln2.setOutline(color_rgb(0, 255, 155))
    ln2.draw(win)

    # line example2
    ln3 = line(300,300,300,400)
    ln3.setOutline(color_rgb(0, 155, 255))
    ln3.setWidth(5)
    ln3.draw(win)


    # rect example
    rec = Rectangle(Point(50, 50), Point(150, 150))
    rec.setOutline(color_rgb(0, 255, 255))
    rec.setFill(color_rgb(255, 100, 50))
    rec.draw(win)

    # circ example
    cir = Circle(Point(350, 250), 100)
    cir.setOutline(color_rgb(0, 255, 255))
    cir.setFill(color_rgb(255, 100, 50))
    cir.setWidth(5)
    cir.draw(win)

    # poly example1
    poly = Polygon(Point(40, 40), Point(100,100), Point(40,100))
    poly.setFill(color_rgb(255, 0, 255))
    poly.draw(win)

    # poly example2
    poly = Polygon(Point(400, 100), 
                   Point(300,200), 
                   Point(200,150), 
                   Point(400,60), 
                   Point(450,100))
    poly.setFill(color_rgb(255, 0, 255))
    poly.setWidth(5)
    poly.draw(win)


    win.getMouse()
    win.close()

main()
