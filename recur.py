
#!/bin/python3

import sys

# consider the recurrence R[i]=(R[i-1] * g + seed) mod p
# R[0], g, seed, p are given
# 
# 1 <= n <= 10^7
# 0 <= s,t < n
# 1 <= p <= n
# 0 <= R[0], g, seed < p
#
#input
# n s t
# 9 0 2
# r[0] g seed p
# 1    3 4    7
# output 
# 2



# def recurrance(n, r_0):
#     global i 
#     global R
#     R[i] = r_0
#  	# R[i]=(R[i-1] * g + seed) mod p
#     print("called with n = %d r_0 = %d  : g, seed, p = , %d, %d, %d " % (n, r_0, g, seed, p))
#     print("called with i = ", i)
#     if (n == 0):
#        print("result 0 = ", r_0)  
#        return r_0
#     # (R[i-1] * g + seed) mod p
#     else:
#         n = n - 1
#         i = i + 1
#         r_0 = (R[i - 1])
#         res = (recurrance(n, r_0) * g + seed ) % p
#         print("result = ", res)
#         return res

# def circularWalk(n, s, t, r_0, g, seed, p):
#     # Complete this function
#     #
#     global i
#     global R 
    
#     i = 0
#     R = []
#     n = n-1
#     ret  = recurrance(n, r_0)
#     return( ret )


#input
# n s t
# 9 0 2
# r[0] g seed p
# 1    3 4    7
# output 
# 2


def circularWalk(n, s, t, r_0, g, seed, p):
    # R[i]=(R[i-1] * g + seed) mod p
    R = [ [-1,"-","-"] for x in range(n)]
    print(R)
    print("called with n=%d r_0=%d  " % (n, r_0) )
    for i in range(0, n):
        if (n == 0):
            R[i][0]=r_0
        else: 
            R[i][0]=int( (R[i-1][0] * g + seed ) % p ) 
        if (i == s ):
            R[i][1]="s"
        if (i == t ):
            R[i][2]="t"  
        print("R[",i,"] = ", R[i][0], R[i][1], R[i][2])

    if ( s == t ):
        return 0

    if ( R[s] == 0): 
        return -1

    j = R[s][0]
    x = 1
    for tri in range(-j, j):
        print("j is ", j)
        if ( t == (s + j) % n ):
            return x    
        x = x + 1 
                
    return(2)    

n, s, t = input().strip().split(' ')
n, s, t = [int(n), int(s), int(t)]
r_0, g, seed, p = input().strip().split(' ')
r_0, g, seed, p = [int(r_0), int(g), int(seed), int(p)]

#R = [-1]*n
print("input: n, s, t, r_0, g, seed, p", n, s, t, r_0, g, seed, p)
result = circularWalk(n, s, t, r_0, g, seed, p)
print(result)


