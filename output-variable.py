name = 'Bob'
print('Hello ' + name)

language = 'Python'
print('I am learning to program '+language)

name = 'Bob'
name = 'Liz'
print('Hello ' + name)

name = 'Bob'
name = name + 'by'
print('Hello ' + name)

name = 'Bob'
name = name + name 
print('Hello ' + name)

name = 'Bob'
name = name + ' ' + name 
print('Hello ' + name)

word1 = 'the '
word2 = 'cat '
word25 = 'sat '
word3 = 'on '
word4 = 'the '
word5 = 'mat'
print(word1 + word2 + word25 + word3 + word4 + word5)


