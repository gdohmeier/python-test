
# -*- coding: utf-8 -*-
""" selection with if exercise """
# the turns off the error complaining about constants
# pylint: disable=C0103


GRADE = int(input('Please enter grade: '))

if GRADE >= 0 and GRADE < 40:
    print'grade = U'
elif GRADE >= 40 and GRADE < 50:
    print'grade = D'
elif GRADE >= 50 and GRADE < 60:
    print'grade = C'
elif GRADE >= 60 and GRADE < 70:
    print'grade = B'
elif GRADE >= 70 and GRADE <= 100:
    print'grade = A'
else:
    print'Invalid entry'


'''
###
##letter = 'b'
##
# if letter=='a':
##    print('This prints if letter is a')
##    print('This prints as well')
# else:
##    print('This prints if the letter is not an a')
##    print('As does this line')
##print('This prints out whatever the letter is')
##
# if letter=='a':
##    print('This prints if letter is a')
##    print('This prints as well')
# elif letter=='b':
##    print('This prints if the letter is b')
# else:
##    print('This prints if the letter is neither a nor b')
##print('This prints out whatever the letter is')
##
##
##x=int(input('Enter a number between 1 and 100: '))
# if x>=1 and x<=100:
##    print('You have entered a valid number')
# else:
##    print('Your number is not valid')

city = input('What is the capital of England?')
if city == 'London':
    print('Well done')
elif city=='Kent':
    print('Right country, wrong city')
elif city=='F':
    print('Terrible joke and wron  answer.')
else:
    print('Sorry wrong answer')'''


one = int(input('Please enter number 1: '))
two = int(input('Please enter number 2: '))
three = int(input('Please enter number 3: '))
if one == two and two == three:
    print'SNAP!'
else:
    print'They do not all match'

